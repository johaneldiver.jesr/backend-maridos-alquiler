const express = require("express");
const {
  addCustomer,
  getCustomer,
  getAllCustomers,
  deleteCustomer,
  updateCustomer,
} = require("../controllers/customer_controller");
const { body } = require("express-validator");

const router = express.Router({ mergeParams: true });

const customerCreationValidators = [
  body("email").notEmpty().isEmail().normalizeEmail(),
  body("name").notEmpty().isString(),
  body("phoneNumber").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = ["email", "name", "phoneNumber"];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

const customerUpdateValidators = [
  body("name").notEmpty().isString(),
  body("phoneNumber").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = ["name", "phoneNumber"];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

router.get("/customers", getAllCustomers);
router.get("/customers/:idCustomer", getCustomer);
router.post("/customers", customerCreationValidators, addCustomer);
router.put("/customers/:idCustomer", customerUpdateValidators, updateCustomer);
router.delete("/customers/:idCustomer", deleteCustomer);

module.exports = router;
