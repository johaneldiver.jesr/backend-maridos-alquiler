const express = require("express");
const {
  addReview,
  getReview,
  getAllReviews,
  getReviewPerHusband,
  deleteReview,
  getReviewPerService,
} = require("../controllers/review_controller");
const { body } = require("express-validator");
const { app } = require("firebase-admin"); 
 
const router = express.Router({ mergeParams: true });

const reviewCreationValidators = [
  body("comment").notEmpty().isString(),
  body("idHusband").notEmpty().isString(), 
  body("idService").notEmpty().isString(),
  body("starRating") 
    .notEmpty()
    .isInt()
    .isIn([0,1, 2, 3, 4, 5])
    .withMessage("Ingresa una calificacion valida"),
  body()
    .custom((body) => {
      const keys = [
        "comment",
        "idHusband",
        "idService",
        "idCategories",
        "starRating",
      ];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

router.post("/review", reviewCreationValidators, addReview);
router.get("/review", getAllReviews);
router.get("/review/:idReview", getReview);
router.get("/review/husbands/:idHusband", getReviewPerHusband);
router.get("/review/services/:idService", getReviewPerService);
router.delete("/review/:idReview", deleteReview);

module.exports = router;
