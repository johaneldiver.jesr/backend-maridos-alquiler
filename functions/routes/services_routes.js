const express = require("express");
const {
  addService,
  getService,
  getAllServices,
  getServicePerCustomer,
  getServicePerHusband,
  getActiveServicesPerCustomer,
  deleteService,
  updateService,
  updateIsActive,
} = require("../controllers/services_controller");
const { body } = require("express-validator");
const { app } = require("firebase-admin");

const router = express.Router({ mergeParams: true });

const serviceCreationValidators = [
  body("address").notEmpty().isString(),
  body("date").notEmpty().isString(),
  body("details").notEmpty().isString(),
  body("idCategory").notEmpty().isString(),
  body("idCustomer").notEmpty().isString(),
  body("isActive").notEmpty().isBoolean(),
  body("time").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = [
        "address",
        "date",
        "details",
        "idCategory",
        "idCustomer",
        "idHusband",
        "isActive",
        "time",
      ];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

const serviceUpdateValidators = [
  body("address").notEmpty().isString(),
  body("date").notEmpty().isString(),
  body("details").notEmpty().isString(),
  body("idCategory").notEmpty().isString(),
  body("idCustomer").notEmpty().isString(),
  body("idHusband").notEmpty().isString(),
  body("isActive").notEmpty().isBoolean(),
  body("time").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = [
        "address",
        "date",
        "details",
        "idCategory",
        "idCustomer",
        "idHusband",
        "isActive",
        "time",
      ];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

const isActiveUpdateValidators = [
  body("isActive").notEmpty().isBoolean(),
  body()
    .custom((body) => {
      const keys = ["isActive"];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

router.post("/service", serviceCreationValidators, addService);
router.get("/services", getAllServices);
router.get("/services/:idService", getService);
router.get("/services/husbands/:idHusband", getServicePerHusband);
router.get("/services/customers/:idCustomer", getServicePerCustomer);
router.get("/services/customers/active/:idCustomer",getActiveServicesPerCustomer);
router.put("/services/:idService", serviceUpdateValidators, updateService);
router.put("/services/isActive/:idService", isActiveUpdateValidators,updateIsActive);
router.delete("/services/:idService", deleteService);

module.exports = router;
