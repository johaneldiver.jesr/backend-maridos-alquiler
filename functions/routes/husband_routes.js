const express = require("express");
const {
  addHusband,
  getHusband,
  getAllHusbands,
  getHusbandsPerCatergory,
  deleteHusband,
  updateHusband,
} = require("../controllers/husband_controller");
const { body } = require("express-validator");

const router = express.Router({ mergeParams: true });

const husbandCreationValidators = [
  body("email").notEmpty().isEmail().normalizeEmail(),
  body("name").notEmpty().isString(),
  body("description").notEmpty().isString(),
  body("idCategories").notEmpty().isArray(),
  body("phoneNumber").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = [
        "email",
        "name",
        "phoneNumber",
        "description",
        "idCategories",
      ];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

const husbandUpdateValidators = [
  body("name").notEmpty().isString(),
  body("description").notEmpty().isString(),
  body("idCategories").notEmpty().isArray(),
  body("phoneNumber").notEmpty().isString(),
  body()
    .custom((body) => {
      const keys = ["name", "phoneNumber", "description", "idCategories"];
      return Object.keys(body).every((key) => keys.includes(key));
    })
    .withMessage("Se agregaron parametros extra"),
];

router.get("/husbands", getAllHusbands);
router.get("/husbands/:idHusband", getHusband);
router.get("/husbands/categories/:idCategory", getHusbandsPerCatergory);
router.post("/husbands", husbandCreationValidators, addHusband);
router.put("/husbands/:idHusband", husbandUpdateValidators, updateHusband);
router.delete("/husbands/:idHusband", deleteHusband);

module.exports = router;
