const express = require("express");
const { getAllCategories } = require("../controllers/categories_controller");
const { body } = require("express-validator");

const router = express.Router({ mergeParams: true });
router.get('/categories', getAllCategories);

module.exports=router;
