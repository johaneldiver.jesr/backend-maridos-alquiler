const admin = require("firebase-admin");
const credentials = require("./credentials");
const db = admin.initializeApp({
  credential: admin.credential.cert(credentials),
});

module.exports = db.firestore();
