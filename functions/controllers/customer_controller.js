const firestore = require("../db");
const { validationResult } = require("express-validator");

const addCustomer = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      //console.log(errors.array())
      return res.status(400).json({ errors: errors.array() });
    }
    const data = req.body;
    await firestore.collection("Customer").doc().set(data);
    res.send("El usuario fue creado correctamente");
    return res.status(204).json();
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};

const getCustomer = async (req, res) => {
  try {
    const id = req.params.idCustomer;
    const Customer = firestore.collection("Customer").doc(id);
    const data = await Customer.get();
    if (!data.exists) {
      res.status(404).send("El usuario no fue encontrado");
    } else {
      const response = data.data();
      //console.log(response)
      return res.status(200).json(response);
    }
  } catch (error) {
    return res.status(500).json();
  }
};

const getAllCustomers = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      //console.log(errors.array())
      return res.status(400).json({ errors: errors.array() });
    }
    const query = firestore.collection("Customer");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const deleteCustomer = async (req, res) => {
  try {
    const id = req.params.idCustomer;
    const doc = firestore.collection("Customer").doc(id);
    await doc.delete();
    return res.status(200).send("El usuario fue borrado con exito");
  } catch (error) {
    return res.status(500).json();
  }
};

const updateCustomer = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors.array());
      return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.idCustomer;
    const data = req.body;
    const Customer = firestore.collection("Customer").doc(id);
    await Customer.update(data);
    res.send("El usuario fue actualizado correctamente");
    return res.status(200).json();
  } catch (error) {
    return res.status(500).json();
  }
};

module.exports = {
  addCustomer,
  getCustomer,
  getAllCustomers,
  deleteCustomer,
  updateCustomer,
};
