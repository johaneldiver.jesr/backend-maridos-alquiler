const firestore = require("../db");
const { validationResult } = require("express-validator");

const addHusband = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      //console.log(errors.array())
      return res.status(400).json({ errors: errors.array() });
    }
    const data = req.body;
    await firestore.collection("Husband").doc().set(data);
    res.send("El marido fue creado correctamente");
    return res.status(204).json();
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};
 
const getHusband = async (req, res) => {
  try {
    const id = req.params.idHusband;
    console.log(id);
    const husband = firestore.collection("Husband").doc(id);
    const data = await husband.get();
    if (!data.exists) {
      res.status(404).send("El marido no fue encontrado");
    } else {
      const response ={
        id: id,
        ...data.data()
      }; 
      return res.status(200).json(response);
    }
  } catch (error) {
    return res.status(500).json();
  }
};

const getAllHusbands = async (req, res) => {
  try {
    const query = firestore.collection("Husband");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const getHusbandsPerCatergory = async (req, res) => {
  try {
    const id = req.params.idCategory;
    const query = firestore
      .collection("Husband")
      .where("idCategories", "array-contains", id);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningún marido con la categoría específicada");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const deleteHusband = async (req, res) => {
  try {
    const id = req.params.idHusband;
    const doc = firestore.collection("Husband").doc(id);
    await doc.delete();
    return res.status(200).send("El Marido fue borrado con exito");
  } catch (error) {
    return res.status(500).json();
  }
};

const updateHusband = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors.array());
      return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.idHusband;
    const data = req.body;
    const husband = firestore.collection("Husband").doc(id);
    await husband.update(data);
    res.send("El Marido fue actualizado correctamente");
    return res.status(200).json();
  } catch (error) {
    return res.status(500).json();
  }
};

module.exports = {
  addHusband,
  getHusband,
  getAllHusbands,
  getHusbandsPerCatergory,
  deleteHusband,
  updateHusband,
};
