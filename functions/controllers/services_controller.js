const firestore = require("../db");
const { validationResult } = require("express-validator");

const addService = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let date = new Date();

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let creationDate;

    if (month < 10) {
      creationDate = `${day}/0${month}/${year}`;
    } else {
      creationDate = `${day}/${month}/${year}`;
    }
    const data = {
      creationDate: creationDate,
      ...req.body,
    };
    const newServiceAdded = await firestore.collection("Services").add(data);
    return res.status(201).json({
      id: newServiceAdded.id,
      msg: "El servicio fue agendado correctamente",
    });
  } catch (error) {
    return res.status(500).send(error);
  }
};

const getService = async (req, res) => {
  try {
    const id = req.params.idService;
    console.log(id);
    const service = firestore.collection("Services").doc(id);
    const data = await service.get();
    if (!data.exists) {
      res.status(404).send("El servicio no fue encontrado");
    } else {
      const response = data.data();

      return res.status(200).json(response);
    }
  } catch (error) {
    return res.status(500).json();
  }
};

const getAllServices = async (req, res) => {
  try {
    const query = firestore.collection("Services");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const getServicePerHusband = async (req, res) => {
  try {
    const id = req.params.idHusband;
    const query = firestore.collection("Services").where("idHusband", "==", id);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningún servicio con el marido específicado");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const getServicePerCustomer = async (req, res) => {
  try {
    const id = req.params.idCustomer;
    const query = firestore
      .collection("Services")
      .where("idCustomer", "==", id);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningún servicio con el cliente específicado");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const getActiveServicesPerCustomer = async (req, res) => {
  try {
    const id = req.params.idCustomer;
    const query = firestore
      .collection("Services")
      .where("idCustomer", "==", id)
      .where("isActive", "==", true);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningún servicio con el cliente específicado");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

//TODO("Como se debería de ver el método")
/* const getActiveServicesPerCustomer = async (req, res) => {
  try {
    const id = req.params.idCustomer;
    const query = firestore
      .collection("Services")
      .where("idCustomer", "==", id)
      .where("isActive", "==", true);

    await query.get()
      .then(async (data) => {
        return data.docs.map(async (doc) => {
          const service = doc.data();
          const husbandQuery = firestore.collection("Husband").doc(service.idHusband);
          const husbandData = await husbandQuery.get();
          const serivceData = {
            husbandName: husbandData.data().name,
            ...service
          }
          return ({
            serivceData
          })
        })
      }).then((data) => {
        if (data.length === 0) {
          return res
            .status(404)
            .send("No se encontró ningún servicio con el cliente específicado");
        } else {
          return res.status(200).json(
            data
          );
        }
      });
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
}; */

const deleteService = async (req, res) => {
  try {
    const id = req.params.idService;
    const doc = firestore.collection("Services").doc(id);
    await doc.delete();
    return res.status(200).send("El servicio fue borrado con exito");
  } catch (error) {
    return res.status(500).json();
  }
};

const updateIsActive = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.idService;
    const data = req.body;
    const Customer = firestore.collection("Services").doc(id);
    await Customer.update(data);
    res.send("El servicio fue actualizado correctamente");
    return res.status(200).json();
  } catch (error) {
    return res.status(500).json();
  }
};

const updateService = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.idService;
    const data = req.body;
    const Customer = firestore.collection("Services").doc(id);
    await Customer.update(data);
    res.send("El servicio fue actualizado correctamente");
    return res.status(200).json();
  } catch (error) {
    return res.status(500).json();
  }
};

module.exports = {
  addService,
  getService,
  getAllServices,
  getServicePerCustomer,
  getServicePerHusband,
  getActiveServicesPerCustomer,
  deleteService,
  updateService,
  updateIsActive,
};
