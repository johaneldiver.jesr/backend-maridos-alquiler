const firestore = require("../db");
const { validationResult } = require("express-validator");

const addReview = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const data = req.body;
    await firestore.collection("Review").doc().set(data);
    res.send("La reseña fue creada correctamente");
    return res.status(204).json();
  } catch (error) {
    return res.status(500).send(error); 
  } 
};
 
const getReview = async (req, res) => {
  try {
    const id = req.params.idReview;
    console.log(id);
    const service = firestore.collection("Review").doc(id);
    const data = await service.get();
    if (!data.exists) {
      res.status(404).send("La reseña no fue encontrada");
    } else {
      const response = data.data();

      return res.status(200).json(response);
    }
  } catch (error) {
    return res.status(500).json();
  }
};

const getAllReviews = async (req, res) => {
  try {
    const query = firestore.collection("Review");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id, 
      ...doc.data(),
    }));
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};
const getReviewPerService = async (req, res) => {
  try {
    const id = req.params.idService;
    const query = firestore.collection("Review").where("idService", "==", id);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningúna reseña con el servicio específicado");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const getReviewPerHusband = async (req, res) => {
  try {
    const id = req.params.idHusband;
    const query = firestore.collection("Review").where("idHusband", "==", id);
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,

      ...doc.data(),
    }));
    if (response.length === 0) {
      return res
        .status(404)
        .send("No se encontró ningúna reseña con el marido específicado");
    }
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

const deleteReview = async (req, res) => {
  try {
    const id = req.params.idReview;
    const doc = firestore.collection("Review").doc(id);
    await doc.delete();
    return res.status(200).send("La reseña fue borrada con exito");
  } catch (error) {
    return res.status(500).json();
  }
};

module.exports = {
  addReview,
  getReview,
  getAllReviews,
  getReviewPerHusband,
  getReviewPerService,
  deleteReview,
};
