const firestore = require("../db");
const { validationResult } = require("express-validator");
const { app } = require("firebase-admin");

const getAllCategories = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const query = firestore.collection("Categories");
    const querySnapshot = await query.get();
    const docs = querySnapshot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json();
  }
};

module.exports = { getAllCategories };
