let chai = require('chai');
let chaiHttp = require('chai-http');
//let server = "https://us-central1-api-fb-3b0eb.cloudfunctions.net/app";
let server = require("../index")
//Asserts
chai.should();
chai.use(chaiHttp);

describe('Customers Api', () => {

    //Get all Customers

    describe('GET /api/customers', () => {
        it('It should get all the Customers', (done) => {
            chai.request(server)
                .get('/api/customers')
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        })
    })

    describe('GET /api/customers', () => {
        it('It should not get any customers', (done) => {
            chai.request(server)
                .get('/api/customer')
                .end((error, response) => {
                    response.should.have.status(404);
                    done();
                })
        })
    })

    //Get customers by id

    describe('GET /api/customers/:idcustomers', () => {
        it('It should get the customer with the given id', (done) => {
            const id = "01"
            chai.request(server)
                .get('/api/customers/' + id)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('email');
                    response.body.should.have.property('name');
                    response.body.should.have.property('phoneNumber');
                    done();
                })
        })
    })

    describe('GET /api/customers/:idcustomers', () => {
        it('It should not get any customers', (done) => {
            const id = "xxxx";
            chai.request(server)
                .get('/api/customers/' + id)
                .end((error, response) => {
                    response.should.have.status(404);
                    response.text.should.be.eq('El usuario no fue encontrado');
                    done();
                })
        })
    })

    //Add customers

    describe('POST /api/customers', () => {
        it('It should post a new customer', (done) => {
            const customers = {
                name: "Juan el cliente Test",
                email: "test@gmail.com",
                phoneNumber: "3333333333",
            }
            chai.request(server)
                .post('/api/customers')
                .send(customers)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El usuario fue creado correctamente');
                    done();
                })
        })
    })

    describe('POST /api/customers', () => {
        it('It should not post a new customer with an empty field', (done) => {
            const customers = {
                name: "",
                email: "test@gmail.com",
                phoneNumber: "3331322301",
            }
            chai.request(server)
                .post('/api/customers')
                .send(customers)
                .end((error, response) => {
                    response.should.have.status(400);
                    response.body.should.have.property('errors')
                    done();
                })
        })
    })

    //Update customers

    describe('PUT /api/customers', () => {
        it('It should update the customer with the given id', (done) => {
            const id = "02"
            const customers = {
                name: "Juan el Customer Cambiado",
                phoneNumber: "3331322301",
            }
            chai.request(server)
                .put('/api/customers/' + id)
                .send(customers)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El usuario fue actualizado correctamente');
                    done();
                })
        })
    })

    describe('PUT /api/customers', () => {
        it('It should not update the customer with an empty field', (done) => {
            const id = "02"
            const customers = {
                name: "Juan el Test Cambiado",
                phoneNumber: "",
            }
            chai.request(server)
                .put('/api/customers/' + id)
                .send(customers)
                .end((error, response) => {
                    response.should.have.status(400);
                    response.body.should.have.property('errors')
                    done();
                })
        })
    })

    //Delete customers

    describe('DELETE /api/customers', () => {
        it('It should delete the customer with the given id', (done) => {
            const id = "zii7N6Rm0uBUq06WJmB9"
            chai.request(server)
                .delete('/api/customers/' + id)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El usuario fue borrado con exito');
                    done();
                })
        })
    })

})