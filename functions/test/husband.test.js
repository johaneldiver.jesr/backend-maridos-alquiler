let chai = require('chai');
let chaiHttp = require('chai-http');
//let server = "https://us-central1-api-fb-3b0eb.cloudfunctions.net/app";
let server = require("../index")

//Asserts
chai.should();
chai.use(chaiHttp);

describe('Husbands Api', () => {

    //Get all Husbands

    describe('GET /api/husbands', () => {
        it('It should get all the husbands', (done) => {
            chai.request(server)
                .get('/api/husbands')
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        })
    })

    describe('GET /api/husbands', () => {
        it('It should not get any husband', (done) => {
            chai.request(server)
                .get('/api/husband')
                .end((error, response) => {
                    response.should.have.status(404);
                    done();
                })
        })
    })

    //Get Husband by id

    describe('GET /api/husbands/:idHusband', () => {
        it('It should get the husband with the given id', (done) => {
            const id = "1020"
            chai.request(server)
                .get('/api/husbands/' + id)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('email');
                    response.body.should.have.property('description');
                    response.body.should.have.property('idCategories');
                    response.body.should.have.property('name');
                    response.body.should.have.property('phoneNumber');
                    done();
                })
        })
    })

    describe('GET /api/husbands/:idHusband', () => {
        it('It should not get any husband', (done) => {
            const id = "xxxx";
            chai.request(server)
                .get('/api/husbands/' + id)
                .end((error, response) => {
                    response.should.have.status(404);
                    response.text.should.be.eq('El marido no fue encontrado');
                    done();
                })
        })
    })

    //Get Husband by category

    describe('GET /api/husbands/categories/:idCategory', () => {
        it('It should get all the husbands with the given category', (done) => {
            const id = '1020';
            chai.request(server)
                .get('/api/husbands/categories/' + id)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    done();
                })
        })
    })

    describe('GET /api/husbands/categories/:idCategory', () => {
        it('It should not get any husband', (done) => {
            const id = 'xxxx';
            chai.request(server)
                .get('/api/husbands/categories/' + id)
                .end((error, response) => {
                    response.should.have.status(404);
                    response.text.should.be.eq('No se encontró ningún marido con la categoría específicada');
                    done();
                })
        })
    })

    //Add Husband

    describe('POST /api/husbands', () => {
        it('It should post a new Husband', (done) => {
            const husband = {
                name: "Juan el Test",
                description: "Soy un test",
                email: "test@gmail.com",
                idCategories: ["1020", "1030"],
                phoneNumber: "3331322301",
            }
            chai.request(server)
                .post('/api/husbands')
                .send(husband)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El marido fue creado correctamente');
                    done();
                })
        })
    })

    describe('POST /api/husbands', () => {
        it('It should not post a new Husband with an empty field', (done) => {
            const husband = {
                name: "",
                description: "Soy un test",
                email: "test@gmail.com",
                idCategories: ["1020", "1030"],
                phoneNumber: "3331322301",
            }
            chai.request(server)
                .post('/api/husbands')
                .send(husband)
                .end((error, response) => {
                    response.should.have.status(400);
                    response.body.should.have.property('errors')
                    done();
                })
        })
    })

    //Update Husband

    describe('PUT /api/husbands', () => {
        it('It should update the Husband with the given id', (done) => {
            const id = "QMoTk2bnSAKuuVtMmsmn"
            const husband = {
                name: "Juan el Test Cambiado",
                description: "Soy un test pero con descripción distinta",
                idCategories: ["1030", "1040"],
                phoneNumber: "3331322301",
            }
            chai.request(server)
                .put('/api/husbands/' + id)
                .send(husband)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El Marido fue actualizado correctamente');
                    done();
                })
        })
    })

    describe('PUT /api/husbands', () => {
        it('It should not update the Husband with an empty field', (done) => {
            const id = "QMoTk2bnSAKuuVtMmsmn"
            const husband = {
                name: "Juan el Test Cambiado",
                description: "Soy un test pero con descripción distinta",
                idCategories: ["1030", "1040"],
                phoneNumber: "",
            }
            chai.request(server)
                .put('/api/husbands/' + id)
                .send(husband)
                .end((error, response) => {
                    response.should.have.status(400);
                    response.body.should.have.property('errors')
                    done();
                })
        })
    })

    //Delete Husband

    describe('DELETE /api/husbands', () => {
        it('It should delete the Husband with the given id', (done) => {
            const id = "lykHUbSczxjD5anA8aP5"
            chai.request(server)
                .delete('/api/husbands/' + id)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.text.should.be.eq('El Marido fue borrado con exito');
                    done();
                })
        })
    })

})