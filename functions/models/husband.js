class Husband {
    constructor(id, name, description, email, idCategories, phoneNumber) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.email = email;
      this.idCategories = idCategories;
      this.phoneNumber = phoneNumber;
    }
  }
  
  module.exports = Husband;
  