const express = require("express");
const functions = require("firebase-functions");
const cors = require("cors");
const app = express();
const husband_routes = require("./routes/husband_routes");
const customer_routes = require("./routes/customer_routes");
const categories_routes = require("./routes/categories_routes");
const review_routes = require("./routes/review_routes");
const services_routes = require("./routes/services_routes");

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

app.use("/api", husband_routes);
app.use("/api", customer_routes);
app.use("/api", categories_routes);
app.use("/api", review_routes);
app.use("/api", services_routes);

exports.app = functions.https.onRequest(app);
