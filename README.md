# **Maridos De Alquiler**

---

El proceso de contratación entre dos partes para la prestación de servicios
(principalmente los del hogar) es llevado a cabo de manera rudimentaria.
Actualmente, los clientes deben obtener el contacto del personal capacitado a
través de terceros, como agendas numéricas, o en los casos más modernos, a
través de internet. Esta contratación generalmente es hecha a través de un
“contrato” verbal, en el que ambas partes pactan una fecha y una hora para
culminar el servicio, sin tener en cuenta aspectos como la calidad del trabajo o la
confiabilidad de las personas involucradas.
Alquiler de Maridos busca automatizar este proceso, creando un sistema de
información al que se pueda ingresar y buscar el servicio deseado, teniendo
acceso a reseñas, información verificada y sin intervención de terceros para la
contratación del servicio, agilizando así la comunicación entre las partes y
brindando seguridad y confiabilidad.

---

## **Guías de código**

<p><a href="https://nodejs.org/en/docs/guides/">Guías de código Node.js<a></p>
<p><a href="https://www.perfomatix.com/nodejs-coding-standards-and-best-practices/">Articulo de buenas prácticas Node.js<a></p>

---

## **Repositorios**

<p><a href="https://gitlab.com/paulinavelasquez2/alquiler-maridos-frontend">Repositorio Aplicativo Web<a></p>
<p><a href="https://gitlab.com/JosePabloRomero/alquiler-de-maridos-app">Repositorio Aplicativo Móvil <a></p>

---
## **Pruebas**

Todas las pruebas pasaron adecuadamente, sin embargo no se muestra la cobertura como actualmente está implementado en el código.
Por lo tanto se adjunta un captura de pantalla que muestra la cobertura:

![image.png](./image.png)
